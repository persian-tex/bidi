# The bidi package
The `bidi` provides a convenient interface for typesetting
bidirectional texts in Plain TeX and LaTeX, using XeTeX engine.

## Donations
If you enjoy the `bidi` package and want to support
the project, then please consider donating to the project.
It allows me to spend more time working on the `bidi` package,
encourages me to continue, and is the perfect way to say
thank you!

You can use [My PayPal.Me](https://www.paypal.me/persiantex) to donate to the `bidi` package. 

If you have a problem using
[My PayPal.Me](https://www.paypal.me/persiantex), then please email me at `persian-tex@tug.org`.

## Issues
If you want to report any bugs or typos and corrections in the documentation,
or ask for any new features, or suggest any improvements, or ask any questions
about the package, then please do not send any direct email to me; I will not
answer any direct email. Instead please use [the issue tracker](https://gitlab.com/persian-tex/bidi/issues).

In doing so, please always explain your issue well enough, always include
a minimal working example showing the issue, and always choose the appropriate
label for your query (i.e. if you are reporting any issues, choose Issue label).

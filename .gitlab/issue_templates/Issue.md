<!---
!! Please fill out all sections !!
-->

## Brief outline of the issue



## Minimal example showing the issue

```tex
\documentclass{article}            % or some other class

  % Any packages other than the bidi package must be loaded here

  % The bidi package must be loaded as the last package
\usepackage[%
    % Any bidi package option goes here
]{bidi}

  % Any preamble code goes here
  
\begin{document}

  % Demonstration of issue here
  
\end{document}
```


## Log and PDF files  

<!---
!! Use drag-and-drop !!
-->
